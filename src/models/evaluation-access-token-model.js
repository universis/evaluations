import {EdmMapping} from '@themost/data';
let AccessToken = require('./access-token-model');
/**
 * @class
 
  * @property {string} access_token
 * @augments {DataObject}
 */
@EdmMapping.entityType('EvaluationAccessToken')
class EvaluationAccessToken extends AccessToken {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    static async inspect(context, access_token) {
        const token = await context.model('EvaluationAccessToken')
          .where('access_token').equal(access_token)
          .silent()
          .getTypedItem();
        if (token == null) {
          return {
            active: false
          }
        }
        return {
          active: !token.isExpired(),
          username: token.user_id,
          client_id: token.client_id,
          access_token: token.access_token,
          refresh_token: token.refresh_token,
          scope: token.scope,
          sub: token.evaluationEvent
        };
      }
}
module.exports = EvaluationAccessToken;
