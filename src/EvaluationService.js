import { ApplicationService } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import {evaluationRouter} from "./evaluationRouter";

function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}

export class EvaluationService extends ApplicationService {
    constructor(app) {
        super(app);
        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                    // get container router
                    const router = container._router;
                    // find after position
                    const before = router.stack.find((item) => {
                        return item.name === 'dataContextMiddleware';
                    });
                    // use eudoxus router
                    container.use('/api/EvaluationEvents/Current', evaluationRouter(app));
                    if (before == null) {
                        // do nothing
                        return;
                    }
                    // get last router
                    const insert = router.stack[router.stack.length - 1];
                    // insert (re-index) router
                    insertRouterBefore(router, before, insert);
                }
            });
        }
    }

}
