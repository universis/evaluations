import { DataObjectState } from '@themost/data';
/**
 * 
 * @param {DataEventArgs} event 
 */
async function beforeSaveAsync(event) {
    // get organizer from courseClass
    const context = event.model.context;
    if (event.target.courseClassInstructor) {
        const organizer = await context.model('CourseClassInstructor')
            .where('id').equal(event.target.courseClassInstructor)
            .select('courseClass/course/department').value();
        event.target.organizer = organizer;
    }
    if (event.state===DataObjectState.Update)
    {
        const previous = event.previous;
        // check if endDate is changed and update expired field for tokens
        if (event.target.endDate) {
            const prevEndDate = event.previous.endDate;
            const endDate = event.target.endDate;
            prevEndDate.setHours(0, 0, 0, 0);
            endDate.setHours(0, 0, 0, 0);
            if (prevEndDate-endDate!==0) {
                let tokens = await context.model('EvaluationAccessToken').where('evaluationEvent').equal(event.target.id)
                    .and('used').notEqual(1)
                    .silent().getItems();
                if (tokens.length > 0) {
                    tokens = tokens.map(x => {
                        x.expires = event.target.endDate;
                        return x;
                    });
                    // update tokens
                    await context.model('EvaluationAccessToken').silent().save(tokens);
                }
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function beforeSave(event, callback) {
    beforeSaveAsync(event).then(() => {
        return callback()
    }).catch( err => {
        return callback(err);
    })
}


export function afterExecute(event, callback) {
    return afterExecuteAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}


export async function afterExecuteAsync(event) {
     // calculate total tokens and total used tokens if events are expanded
    if (event.emitter && event.emitter.$expand && Array.isArray(event.emitter.$expand)) {
        // if tokens of classInstructorEvaluationEvents are expanded and $select and $groupby are present to mapping options
        if (event.emitter.$expand.find(mapping => {
            // $select and $groupby are present to mapping options
            return typeof mapping === 'object'
                ? mapping.name === 'tokens' && (mapping.options && mapping.options.$select && mapping.options.$groupby)
                : mapping === 'tokens'
        })) {
            const results = Array.isArray(event.result) ? event.result : [event.result]
            for (const result of results) {
                if (Array.isArray(result.tokens) && result.tokens.length) {
                    // calculate total and used tokens
                    result.calculatedTotal = result.tokens.reduce((partial_sum, a) => partial_sum + a.total, 0)
                    const findUsed = result.tokens.find(x=>{
                        return x.used;
                    });
                    result.calculatedUsed = findUsed?findUsed.total:0;
                }
            }
        }
    }
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeRemoveAsync(event) {
    // ensure associations
    const context = event.model.context;
    const evaluationEvent = context.model('EvaluationEvent').convert(event.target);
    const form = await evaluationEvent.property('evaluationDocument').getItem();
    if (form && form.resultType) {
        if (context.model(form.resultType) == null) {
            // ensure model is added to configuration
            await evaluationEvent.getForm();
        }
        const answers = await context.model(form.resultType)
            .where('evaluationEvent').equal(event.target.id)
            .silent().count();
        if (answers) {
            throw new Error('Cannot remove evaluation event since there are responses associated with it')
        }
    }
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    // execute async method
    return beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

